# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Code for Multi-Scale Timber Element analysis with Bayesin Networks
* V3.1

### How do I get set up? ###

* The code bnlearn R library from Marco Scutari along with other packages are used.
* Makes use of processed data stored in ./processed/preparedData.rds
* Run by simply sourcing R scripts in the following order:
* 1) unsupervisedPhase.r
* 2) supervisedPhase.r
* 3) customeNetwork.r
* 4) plots.r

### Who do I talk to? ###

* Francisco Prieto-Castrillo. franciscop@usal.es
