#plots
#INIT
rm(list=ls(all=TRUE))
#install required libraries
for (package in c("bnlearn", "gRain","Rgraphviz","parallel")) {
	if (!require(package, character.only=T, quietly=T)) {
		install.packages(package)
		library(package, character.only=T)
	}
}


## load unsup fit
unsupfitmodels <- readRDS(file = "processed/unsupfitmodels.rds")
## load sup fit
supfitmodels <- readRDS(file = "processed/supfitmodels.rds")
## load op fit
fitOp <- readRDS(file = "processed/fitOp.rds")

#extract best models for phase1 and 2
unsupfit<-unsupfitmodels$bnSB_HC_BDE
supfit<-supfitmodels$bnSB_HC_AIC
custfit<-fitOp

#functions
getConds<-function(v)
{
	cUN<-querygrain(junctionUN, nodes = c("MOE_glob",v),type="conditional")
	cUN<-cUN[,c(4,3,1,2)] #ordenar 
	rownames(cUN)<-sapply(rownames(cUN),function(x) paste0(x,"_UN"))
	cS<-querygrain(junctionS, nodes = c("MOE_glob",v),type="conditional")
	cS<-cS[,c(4,3,1,2)]
	rownames(cS)<-sapply(rownames(cS),function(x) paste0(x,"_S"))
	cOP<-querygrain(junctionOP, nodes = c("MOE_glob",v),type="conditional")
	cOP<-cOP[,c(4,3,1,2)]
	rownames(cOP)<-sapply(rownames(cOP),function(x) paste0(x,"_OP"))
	cAll<-rbind(cUN,cS,cOP)
	#trasponse
	cAll<-t(cAll)
	return(cAll)
}


#junction tree compilation
junctionUN <- compile(as.grain(unsupfit))
junctionS <- compile(as.grain(supfit))
junctionOP <- compile(as.grain(custfit))

#obtain the conditionals
condScale<-getConds("Scale") 
condElement<-getConds("Element")
condMOE_loc<-getConds("MOE_loc")
condVis<-getConds("Vis..Insp")
#sort columns
condMOE_loc<-condMOE_loc[,as.numeric(sapply(1:3,function(x) 4*(x-1)+c(3,2,1,4)))]
#change MOE_glob rownames
rownames(condScale)<-c("13.4k-17.7k","11.9k-13.4k","10.1k-11.9k","1.1k-10.1k")
rownames(condElement)<-c("13.4k-17.7k","11.9k-13.4k","10.1k-11.9k","1.1k-10.1k")
rownames(condMOE_loc)<-c("13.4k-17.7k","11.9k-13.4k","10.1k-11.9k","1.1k-10.1k")
rownames(condVis)<-c("13.4k-17.7k","11.9k-13.4k","10.1k-11.9k","1.1k-10.1k")


#Plot 6
par(mar=c(14,4,4,4))

MOEglobColors<-c("blue","lightblue","cyan","green")
unsup<-condVis[,1:4]
sup<-condVis[,5:8]
op<-condVis[,9:12]
matplot(t(unsup),type="o",lwd=3,lty=1,col=MOEglobColors,ylim=c(0,1),pch=20,ylab="",axes=F)
axis(2)
axis(1,at=1:4,labels=c("I","II","III","NC"),las=3)
matplot(t(sup),type="o",lwd=3,lty=2,col=MOEglobColors,ylim=c(0,1),pch=20,ylab="",axes=F,add=T)
matplot(t(op),type="o",lwd=3,lty=3,col=MOEglobColors,ylim=c(0,1),pch=20,ylab="",axes=F,add=T)
box()
legend("topleft",c("unsupervised","supervised","operative"),
lty=c(1,2,3),lwd=c(3,3,3)
	)
	
#Plot 7
.newGraphics()
par(mar=c(14,4,4,4))

colnames(condMOE_loc)<-as.character(outer(c("15.2k-32.3k","12.8k-15.2k","10.5k-12.8k","2.6k-10.5k"),c("_UN","_S","_OP"),FUN="paste0"))
MOEglobColors<-c("blue","lightblue","cyan","green")
unsup<-condMOE_loc[,1:4]
sup<-condMOE_loc[,5:8]
op<-condMOE_loc[,9:12]
matplot(unsup,type="o",lwd=3,lty=1,col=MOEglobColors,ylim=c(0,1),pch=20,ylab="",axes=F)
axis(2)
axis(1,at=1:4,labels=c("15.2k-32.3k","12.8k-15.2k","10.5k-12.8k","2.6k-10.5k"),las=3)
matplot(sup,type="o",lwd=3,lty=2,col=MOEglobColors,ylim=c(0,1),pch=20,ylab="",axes=F,add=T)
matplot(op,type="o",lwd=3,lty=3,col=MOEglobColors,ylim=c(0,1),pch=20,ylab="",axes=F,add=T)
box()
legend("topleft",c("unsupervised","supervised","operative"),
lty=c(1,2,3),lwd=c(3,3,3)
	)
#to show legend color bar
#barplot(rep(1,4),col=MOEglobColors,space=0,beside=T)
