#phase 2
#INIT
rm(list=ls(all=TRUE))
#install required libraries
for (package in c("bnlearn", "gRain","Rgraphviz","parallel")) {
	if (!require(package, character.only=T, quietly=T)) {
		install.packages(package)
		library(package, character.only=T)
	}
}

## functions
kld<-function(modelfit,D,smoothEmp)
{
	#empirical freqs.
	efreqs<-as.data.frame(table(D))
	efreqs<-data.frame(efreqs,eprob=efreqs$Freq/nrow(D)+smoothEmp)
	#create the mode joint prob.
	fjoint<-querygrain(as.grain(modelfit),type="joint")
	fdistentries<-t(sapply(1:nrow(efreqs),function(n) as.numeric(sapply(names(dimnames(fjoint)),function(x) which(dimnames(fjoint)[[x]]==efreqs[n,x])))))
	fjoint<-apply(fdistentries,1,function(x) fjoint[matrix(x,nrow=1)])
	#merge with the data probs
	mergedjoints<-data.frame(efreqs,mfreqs=fjoint)
	#drop 0 entries
	#mergedjoints<-mergedjoints[mergedjoints$Freq>0,]
	#KLD
	KLD<-with(mergedjoints,sum(eprob*log10(eprob/mfreqs)))
	return(KLD)
}
setRootNode<-function(dd,rootnode)
{
	otherVars<-names(dd)[-which(names(dd)==rootnode)]
	#add all other variables from to this node to the bl
	blRoot<-as.matrix(data.frame(from=otherVars,to=rep(rootnode,length(otherVars))))
	return(blRoot)
}

setRootNodes<-function(dd,rootnodes)
{
	bls<-lapply(rootnodes,function(node) setRootNode(dd,node))
	blRoots<-do.call(rbind,bls)
	return(blRoots)
}

createBlackList<-function(dd,isolatednodes,rootnodes,blCustom)
{
	#blacklist adds:
	#1 isolated nodes
	blIsolated<-setIsolatedNodes(dd,isolatednodes)
	#2 root nodes
	blRoot<-setRootNodes(dd,rootnodes)
	#3 other manually specified arcs imported as blCustom
	bls<-list(blIsolated,blRoot,blCustom)
	bl<-do.call(rbind,bls)
	return(bl)
}
createWhiteList<-function(wl)
{
	#unnecesary in this analysis.
	return(wl)
}
setIsolatedNode<-function(dd,isonode)
{
	otherVars<-names(dd)[-which(names(dd)==isonode)]
	#add all other variables from to this node to the bl
	blFrom<-as.matrix(data.frame(from=otherVars,to=rep(isonode,length(otherVars))))
	#add all other variables to to this node to the bl
	blTo<-as.matrix(data.frame(from=rep(isonode,length(otherVars)),to=otherVars))
	blIso<-rbind(blFrom,blTo)
	return(blIso)
}


setIsolatedNodes<-function(dd,isonodes)
{
	#get all bls from these nodes
	bls<-lapply(isonodes,function(node) setIsolatedNode(dd,node))
	blIsolated<-do.call(rbind,bls)
	return(blIsolated)
}

isolatedVarInBlackList<-function(dd,var,bbl)
{
	#for a given variable create a list from/to that variable
	onames<-names(dd)[-which(names(dd)==var)]
	fromlist<-data.frame(from=rep(var,length(onames)),to=onames,stringsAsFactors = F)
	tolist<-data.frame(from=fromlist$to,to=fromlist$from,stringsAsFactors = F)
	fromtolist<-rbind(fromlist,tolist)
	#check if all these constraints are in the bl
	isolatedFromBL<-all(sapply(1:nrow(fromtolist),function(n) any(apply(bbl,1,function(r) (fromtolist[n,1]==r[1] & fromtolist[n,2]==r[2])))))
	return(isolatedFromBL)
}
checkConstraintsConsistency<-function(dd,bbl,wwl)
{
	#first check if resulting isolated nodes are in fact dependent of any other variable
	#from the blacklist check if isolated nodes have any dependency
	isolatedNodesFromBL<-names(which(sapply(names(dd),function(var) isolatedVarInBlackList(dd,var,bbl))))
	#for every isolated node check if there is any other dependency between that node and some variable
	isolatedDeps<-lapply(isolatedNodesFromBL,function(x) getCondDependencies(dd,x,0.05))
	names(isolatedDeps)<-isolatedNodesFromBL
	#see if any intendend isolated has in fact ci dependents
	reviewIsolated<-any(sapply(isolatedDeps,function(x) length(x)>0))
	#if TRUE, expert must review constraints in blacklist
	if(reviewIsolated){
		isolatesToReview1<-isolatedDeps[sapply(isolatedDeps,function(x) length(x)>0)]
		#and compute also the p-value for every isolated node and its dependents
		isolatesToReview<-lapply(1:length(isolatesToReview1), function(n) sapply(isolatesToReview1[n][[1]],function(x) ci.test(names(isolatesToReview1[n]),x,data = dd,test = "x2")$p.value))
		names(isolatesToReview)<-names(isolatesToReview1)
	}else{isolatesToReview<-NULL}
	#check if edges in white list must be independent.
	if(!is.null(wwl))
	{
		# Ho: A indep B, pval<=0.05, A,B dep.
		wlCItests<-data.frame(cbind(wwl,apply(wwl,1,function(x) ci.test(x[1],x[2],data=dd,test="x2")$p.value)),stringsAsFactors = F)
		names(wlCItests)<-c("from","to","p.value")
		checkDependentsFromWL<-any(as.numeric(wlCItests$p.value) > 0.05) #if true, expert must review constraints in wl
		if(checkDependentsFromWL){
			dependenciesToReview<-wlCItests[which(as.numeric(wlCItests$p.value) > 0.05),]
		}else{dependenciesToReview<-NULL}
	}else{checkDependentsFromWL<-FALSE;dependenciesToReview<-NULL}
	
	return(list(isolatedNodesFromBL=isolatedNodesFromBL,reviewIsolated=reviewIsolated,isolatesToReview=isolatesToReview,checkDependentsFromWL=checkDependentsFromWL,dependenciesToReview=dependenciesToReview))
}


## parameters
set.seed(100)
bootN <- 10000
bootETH <- 0.5
bootDTH <- 0.5
alpha <- 0.05
restart <- 10
perturb <- 10
datatestprop <- 0.33
kgroups <- 10
smoothEmp <- 1e-10
Bayesiss <- 1
rankers <- c("errorranks","kldrank")
acores<-detectCores()-1

## load prepared data
preparedData <- readRDS(file = "processed/preparedData.rds")

#split data
testsamples<-sample(1:nrow(preparedData),size = round(datatestprop*nrow(preparedData)))
testdata<-preparedData[testsamples,]
traindata<-preparedData[-testsamples,]

## supervision
isolatedNodes <- NULL
rootNodes <- c("Element","Scale")
customBL <- NULL
wl <- NULL
isolatedNodes <- NA

#drop Section
traindata<-traindata[,-which(names(traindata)=="Section")]
testdata<-testdata[,-which(names(testdata)=="Section")]
preparedData<-preparedData[,-which(names(preparedData)=="Section")]

## createConstraints
### createBlackList
bl <- createBlackList(dd = traindata, isolatednodes = isolatedNodes, rootnodes = rootNodes, blCustom = customBL)

### createWhiteList
wl <- createWhiteList(wl = wl)

## checkConstraintsConsistency
constraintsConsistency <- checkConstraintsConsistency(dd = traindata, bbl = bl, wwl = wl)

#constr. consistent
 if(constraintsConsistency$reviewIsolated){
    cat("proposed isolated nodes are not consistent with data. x2 p.value with H0: A indep B \n")
    print(constraintsConsistency$isolatesToReview)
    #stop("review proposed isolated nodes")
    }
  if(constraintsConsistency$checkDependentsFromWL){
    cat("suggested direct dependencies in white list are not consistent with data. x2 p.value with H0: A indep B \n")
    print(constraintsConsistency$dependenciesToReview)
    #stop("review proposed dependencies: they seem to be independencies")
  }
  if(constraintsConsistency$reviewIsolated | constraintsConsistency$checkDependentsFromWL){stop("check constraints!")}

## generate models
#bnCS_GS_X2
bnCS_GS_X2<-gs(traindata,test = "x2",undirected = F,alpha = alpha,strict = F,blacklist = bl,whitelist = wl)

#bnCS_GS_MI
bnCS_GS_MI<-gs(traindata,test = "mi",undirected = F,alpha = alpha,strict = F,blacklist = bl,whitelist = wl)

# bnCS_IAMB_X2
bnCS_IAMB_X2<-iamb(traindata,test = "x2",undirected = F,alpha = alpha,strict = F,blacklist = bl,whitelist = wl)

# bnCS_IAMB_MI
bnCS_IAMB_MI<-iamb(traindata,test = "mi",undirected = F,alpha = alpha,strict = F,blacklist = bl,whitelist = wl)

#bnCS_HITON_PC_X2
bnCS_HITON_PC_X2<-si.hiton.pc(traindata,test = "x2",alpha = alpha,strict = F,blacklist = bl,whitelist = wl)

#bnCS_HITON_PC_MI
bnCS_HITON_PC_MI<-si.hiton.pc(traindata,test = "mi",alpha = alpha,strict = F,blacklist = bl,whitelist = wl)

#bnSB_HC_LL
bnSB_HC_LL<-hc(traindata,score="loglik",restart = restart,perturb = perturb,blacklist = bl,whitelist = wl)

#bnSB_HC_BIC
bnSB_HC_BIC<-hc(traindata,score="bic",restart = restart,perturb = perturb,blacklist = bl,whitelist = wl)

#bnSB_HC_AIC
bnSB_HC_AIC<-hc(traindata,score="aic",restart = restart,perturb = perturb,blacklist = bl,whitelist = wl)

#bnSB_HC_BDE
bnSB_HC_BDE<-hc(traindata,score="bde",restart = restart,perturb = perturb,blacklist = bl,whitelist = wl)

#bnSB_TABU_LL
bnSB_TABU_LL<-tabu(traindata,score="loglik",blacklist = bl,whitelist = wl)

#bnSB_TABU_BIC
bnSB_TABU_BIC<-tabu(traindata,score="bic",blacklist = bl,whitelist = wl)

#bnSB_TABU_AIC
bnSB_TABU_AIC<-tabu(traindata,score="aic",blacklist = bl,whitelist = wl)

#bnSB_TABU_BDE
bnSB_TABU_BDE<-tabu(traindata,score="bde",blacklist = bl,whitelist = wl)

bnH_MMHC_X2<-mmhc(traindata,test = "x2",alpha = alpha,strict = F,blacklist = bl,whitelist = wl,restart = restart,perturb = perturb)

bnH_MMHC_MI<-mmhc(traindata,test = "mi",alpha = alpha,strict = F,blacklist = bl,whitelist = wl,restart = restart,perturb = perturb)

#set modelNames
modelNames<-names(which(sapply(ls(),function(x) length(grep("bn",x)) > 0)))

#set models
models<-lapply(modelNames,get)
names(models)<-modelNames

#convert to dir
dirmodels<-lapply(models,function(x) {
	ifelse(!directed(x),net<-try(cextend(x,strict = F),silent=F),net<-x)
	;return(net)
	})
#rule out undirected models since there is no consistency in cextending them
dirmodels<-dirmodels[sapply(dirmodels,directed)]

#cvpreds: k-fold prediction error

cvpredsMean<-sapply(dirmodels,function(x) mean(sapply(nodes(x),function(tn) attr(bn.cv(preparedData,bn=x,loss="pred",loss.args=list(target=tn),fit="bayes",fit.args=list(iss=Bayesiss), method = "hold-out",m=round(datatestprop*nrow(preparedData)),k=kgroups),"mean"))))


cvpredsMean<-cvpredsMean[order(cvpredsMean)]
cvpredsMean<-data.frame(alg=names(cvpredsMean),predErr=as.numeric(cvpredsMean))


#fit models
fitmodels<-lapply(dirmodels,function(x) bn.fit(x,data = traindata[,which(names(traindata) %in% nodes(x))],method="bayes",iss=Bayesiss))

#nodes and arcs
nodesarcs<-t(sapply(fitmodels,function(x) c(length(nodes(x)),nrow(arcs(x)))))
nodesarcs<-data.frame(alg=rownames(nodesarcs),nodes=nodesarcs[,1],arcs=nodesarcs[,2])
#sort by arcs
nodesarcs<-nodesarcs[order(nodesarcs$arcs,decreasing = T),]
rownames(nodesarcs)<-NULL

## merge errPred
errscores <- merge(x = nodesarcs, y = cvpredsMean, all = FALSE, by = c("alg"))

#kld
KLDsup<-mclapply(fitmodels,function(x) kld(x,testdata,smoothEmp),mc.cores=acores)
KLDsup<-do.call(rbind,KLDsup)
KLDsup<-data.frame(alg=rownames(KLDsup),kld=as.numeric(KLDsup))
KLDsup<-KLDsup[order(KLDsup$kld),]
rownames(KLDsup)<-NULL

## merge KLD
kldscores <- merge(x = errscores, y = KLDsup, all = FALSE, by = c("alg"))

#scores
sumLL<-sapply(dirmodels,function(x) bnlearn::score(x,testdata,type="loglik"))
#bic
bic<-sapply(dirmodels,function(x) bnlearn::score(x,testdata,type="bic"))
#aic
aic<-sapply(dirmodels,function(x) bnlearn::score(x,testdata,type="aic"))
#bde
bde<-sapply(dirmodels,function(x) bnlearn::score(x,testdata,type="bde"))
modelscores<-data.frame(alg=names(dirmodels),sumLL=sumLL,bic=bic,aic=aic,bde=bde)


## merge scores
modelscores <- merge(x = kldscores, y = modelscores, all = FALSE)

#rank
#first set precission in kld and predErr to 2 decimals
#since max precission we have in data is 1883,77
modelscores$predErr<-round(modelscores$predErr,2)
modelscores$kld<-round(modelscores$kld,2)
#do not rank kld
rankedscores<-cbind(modelscores[,c(1:5)],apply(modelscores[,-c(1:5)],2,function(v) sapply(v,function(x) which(x==sort(unique(v),decreasing = T)))))
#add error rankings
errorranks<-sapply(rankedscores$predErr,function(x) which(x==sort(unique(rankedscores$predErr))))
kldrank<-sapply(rankedscores$kld,function(x) which(x==sort(unique(rankedscores$kld))))
rankedscores<-cbind(rankedscores[,1:5],errorranks=errorranks,kldrank=kldrank,rankedscores[,-c(1:5)])
#totalranks is sum of errorranks+kldranks - min(errorranks+kldranks) + 1 
totalranks<-data.frame(rankedscores,totalrank=rowSums(with(rankedscores,rankedscores[,rankers]))-min(rowSums(with(rankedscores,rankedscores[,rankers])))+1)
totalranks<-totalranks[order(totalranks$totalrank),]
rownames(totalranks)<-NULL
totalranks<-totalranks[,-2]

#show only signif columns

totalranks<-totalranks[,c(1,2,5,6,8:11)]

#best representants
bestrepNames<-as.character(sapply(c("bnSB","bnCS","bnH"),function(v) as.character(totalranks$alg[which(sapply(totalranks$alg,function(x) length(grep(v,x))==1))[1]])))
bestrep<-models[bestrepNames]


#select seed model. This is a supervised step. In this case it coincides with "bnSB_HC_AIC"
seedmodel<-get(as.character(totalranks$alg[1]))

#boot
bootmodel <- (function(net,bootn){
#constraint based algs have test instead of score arg
if(net$learning$algo %in% c("gs","iamb","fast.iamb","inter.iamb","mmpc","si.hiton.pc")){
	algArgs<-list(test=net$learning$test,whitelist=net$learning$whitelist,blacklist=net$learning$blacklist)}else{
		algArgs<-list(score=net$learning$test,whitelist=net$learning$whitelist,blacklist=net$learning$blacklist)}
cat("creating ",bootn, "instances for: ",paste(net$learning[["algo"]],net$learning[["test"]],sep = "_"), "\n")
if(as.logical(Sys.info()["nodename"]!="Franciscos-MacBook-Pro.local")){cl = makeCluster((detectCores()-1), type = "MPI");bootModel<-boot.strength(traindata,R=bootn,algorithm=net$learning$algo,algorithm.args = algArgs,cluster = cl);stopCluster(cl)}else{cat("booting no parallel \n");bootModel<-boot.strength(traindata,R=bootn,algorithm=net$learning$algo,algorithm.args = algArgs)}
 bootModel<-bootModel[order(bootModel$strength,decreasing = T),]
 return(bootModel)
})(net = seedmodel, bootn = bootN)

#crop averaged net
bootModel_S<-bootmodel[bootmodel$strength>bootETH & bootmodel$direction>bootDTH,]

#avg network
avg_S <- (function(bm){
  #attr(bootModel,"method")<-attr(bootModel,"mode")
  avg.boot <- averaged.network(bm,threshold = bootETH,nodes = names(traindata))
  return(avg.boot)
})(bm = bootModel_S)

## convertToDirected
dirAvgNet <- (function(net){
	if(!directed(net)){net<-cextend(net)}
	return(net)
})(net = avg_S)

#fit avgNet
fitAvgNet<-bn.fit(dirAvgNet,data = traindata[,which(names(traindata) %in% nodes(dirAvgNet))],method="bayes",iss=Bayesiss)

#KLD avgNet
KLDAvg<-kld(fitAvgNet,testdata,smoothEmp)
#round to 2
KLDAvg<-round(KLDAvg,2)

#avg Net scores
dirmodels<-list(dirAvgNet=dirAvgNet)
dd<-testdata[,which(names(testdata) %in% nodes(dirAvgNet))]
sumLL<-sapply(dirmodels,function(x) bnlearn::score(x,dd,type="loglik"))
#bic
bic<-sapply(dirmodels,function(x) bnlearn::score(x,dd,type="bic"))
#aic
aic<-sapply(dirmodels,function(x) bnlearn::score(x,dd,type="aic"))
#bde
bde<-sapply(dirmodels,function(x) bnlearn::score(x,dd,type="bde"))
modelscoresAvg<-data.frame(alg=names(dirmodels),nodes=length(nodes(dirAvgNet)),arcs=nrow(arcs(dirAvgNet)),sumLL=sumLL,bic=bic,aic=aic,bde=bde)


#cv Avg Pred error
dd<-preparedData[,which(names(preparedData) %in% nodes(dirAvgNet))]
errAvg<-mean(sapply(nodes(dirAvgNet),function(x) attr(bn.cv(dd,bn=dirAvgNet,loss="pred",loss.args=list(target=x),fit="bayes",fit.args = list(iss=Bayesiss), method = "hold-out",m=round(datatestprop*nrow(dd)),k=kgroups),"mean")))
#round errAvg to precisison
errAvg<-round(errAvg,2)

#combine with previous scores
#first add avg kld column
modelscoresAvg<-data.frame(modelscoresAvg[,c(1:3)],predErr=errAvg,kld=as.numeric(KLDAvg),modelscoresAvg[,-c(1:3)])
#add avg net error
modelscoresAvg<-rbind(modelscores,modelscoresAvg)

#rank
#set to max data precisison
modelscoresAvg$predErr<-round(modelscoresAvg$predErr,2)
#do not rank kld
rankedscoresAvg<-cbind(modelscoresAvg[,1:5],apply(modelscoresAvg[,-c(1:5)],2,function(v) sapply(v,function(x) which(x==sort(unique(v),decreasing = T)))))
#add error rankings
errPredRanks<-sapply(rankedscoresAvg$predErr,function(x) which(x==sort(unique(rankedscoresAvg$predErr))))
kldrank<-sapply(rankedscoresAvg$kld,function(x) which(x==sort(unique(rankedscoresAvg$kld))))
rankedscoresAvg<-cbind(rankedscoresAvg[,1:5],errorranks=errPredRanks,kldrank=kldrank,rankedscoresAvg[,-c(1:5)])

totalranksAvg<-data.frame(rankedscoresAvg,totalrank=rowSums(with(rankedscoresAvg,rankedscoresAvg[,rankers]))-min(rowSums(with(rankedscoresAvg,rankedscoresAvg[,rankers])))+1)
#totalranksAvgOp<-data.frame(rankedscoresAvgOp,totalrank=rankedscoresAvgOp$kldrank-min(rankedscoresAvgOp$kldrank)+1)
totalranksAvg<-totalranksAvg[order(totalranksAvg$totalrank),]

#drop nodes column
totalranksAvg<-totalranksAvg[,-2]
totalranksAvg<-totalranksAvg[,c(1,2,5,6,7,8:11)]

