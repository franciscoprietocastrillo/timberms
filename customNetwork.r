#custome network
cust <- empty.graph(nodes = names(preparedData))
cust <- set.arc(cust, from = "Scale", to = "Vis..Insp")
cust <- set.arc(cust, from = "Scale", to = "MOE_loc")
cust <- set.arc(cust, from = "MOE_loc", to = "MOE_glob")
cust <- set.arc(cust, from = "Vis..Insp", to = "ultrasound")
cust <- set.arc(cust, from = "Vis..Insp", to = "MOE_glob")

#fit operative
fitOp<-bn.fit(cust,data = traindata[,which(names(traindata) %in% nodes(cust))],method="bayes",iss=Bayesiss)

#KLD operative
KLDop<-kld(fitOp,testdata,smoothEmp)
#round to max precission
KLDop<-round(KLDop,2)

#operative scores
dirmodels<-list(operational=cust)
dd<-testdata[,which(names(testdata) %in% nodes(cust))]
sumLL<-sapply(dirmodels,function(x) bnlearn::score(x,dd,type="loglik"))
#bic
bic<-sapply(dirmodels,function(x) bnlearn::score(x,dd,type="bic"))
#aic
aic<-sapply(dirmodels,function(x) bnlearn::score(x,dd,type="aic"))
#bde
bde<-sapply(dirmodels,function(x) bnlearn::score(x,dd,type="bde"))
modelscoresAvgOp<-data.frame(alg=names(dirmodels),nodes=length(nodes(cust)),arcs=nrow(arcs(cust)),sumLL=sumLL,bic=bic,aic=aic,bde=bde)

#cv op. Pred error
dd<-preparedData[,which(names(preparedData) %in% nodes(cust))]
errOp<-mean(sapply(nodes(cust),function(x) attr(bn.cv(dd,bn=cust,loss="pred",loss.args=list(target=x),fit="bayes",fit.args = list(iss=Bayesiss), method = "hold-out",m=round(datatestprop*nrow(dd)),k=kgroups),"mean")))

#combine with previous scores
#first add avg kld column
modelscoresAvgOp<-data.frame(modelscoresAvgOp[,c(1:3)],predErr=errOp,kld=as.numeric(KLDop),modelscoresAvgOp[,-c(1:3)])
modelscoresAvgOp<-rbind(modelscoresAvg,modelscoresAvgOp)

#rank
#set to max data precisison
modelscoresAvgOp$predErr<-round(modelscoresAvgOp$predErr,2)
#do not rank kld
rankedscoresAvgOp<-cbind(modelscoresAvgOp[,1:5],apply(modelscoresAvgOp[,-c(1:5)],2,function(v) sapply(v,function(x) which(x==sort(unique(v),decreasing = T)))))
#add error rankings
errPredRanksOp<-sapply(rankedscoresAvgOp$predErr,function(x) which(x==sort(unique(rankedscoresAvgOp$predErr))))
kldrank<-sapply(rankedscoresAvgOp$kld,function(x) which(x==sort(unique(rankedscoresAvgOp$kld))))
rankedscoresAvgOp<-cbind(rankedscoresAvgOp[,1:5],errorranks=errPredRanksOp,kldrank=kldrank,rankedscoresAvgOp[,-c(1:5)])

totalranksAvgOp<-data.frame(rankedscoresAvgOp,totalrank=rowSums(with(rankedscoresAvgOp,rankedscoresAvgOp[,rankers]))-min(rowSums(with(rankedscoresAvgOp,rankedscoresAvgOp[,rankers])))+1)
totalranksAvgOp<-totalranksAvgOp[order(totalranksAvgOp$totalrank),]

#drop nodes column
totalranksAvgOp<-totalranksAvgOp[,-2]
totalranksAvgOp<-totalranksAvgOp[,c(1,2,5,6,7,8:11)]

