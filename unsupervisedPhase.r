#phase 1
#INIT
rm(list=ls(all=TRUE))
#install required libraries
for (package in c("bnlearn", "gRain","Rgraphviz","parallel")) {
	if (!require(package, character.only=T, quietly=T)) {
		install.packages(package)
		library(package, character.only=T)
	}
}

## functions
kld<-function(modelfit,D,smoothEmp)
{
	#empirical freqs.
	efreqs<-as.data.frame(table(D))
	efreqs<-data.frame(efreqs,eprob=efreqs$Freq/nrow(D)+smoothEmp)
	#create the mode joint prob.
	fjoint<-querygrain(as.grain(modelfit),type="joint")
	fdistentries<-t(sapply(1:nrow(efreqs),function(n) as.numeric(sapply(names(dimnames(fjoint)),function(x) which(dimnames(fjoint)[[x]]==efreqs[n,x])))))
	fjoint<-apply(fdistentries,1,function(x) fjoint[matrix(x,nrow=1)])
	#merge with the data probs
	mergedjoints<-data.frame(efreqs,mfreqs=fjoint)
	#drop 0 entries
	#mergedjoints<-mergedjoints[mergedjoints$Freq>0,]
	#KLD
	KLD<-with(mergedjoints,sum(eprob*log10(eprob/mfreqs)))
	return(KLD)
}

## parameters
set.seed(100)
bootN <- 10000
bootETH <- 0.5
bootDTH <- 0.5
alpha <- 0.05
restart <- 10
perturb <- 10
datatestprop <- 0.33
kgroups <- 10
smoothEmp <- 1e-10
Bayesiss <- 1
rankers <- c("errorranks","kldrank")
acores<-detectCores()-1
## load prepared data
preparedData <- readRDS(file = "processed/preparedData.rds")

#split data
testsamples<-sample(1:nrow(preparedData),size = round(datatestprop*nrow(preparedData)))
testdata<-preparedData[testsamples,]
traindata<-preparedData[-testsamples,]

## unsupervised
bl<-NULL;wl<-NULL

## generate models
#bnCS_GS_X2
bnCS_GS_X2<-gs(traindata,test = "x2",undirected = F,alpha = alpha,strict = F,blacklist = bl,whitelist = wl)

#bnCS_GS_MI
bnCS_GS_MI<-gs(traindata,test = "mi",undirected = F,alpha = alpha,strict = F,blacklist = bl,whitelist = wl)

# bnCS_IAMB_X2
bnCS_IAMB_X2<-iamb(traindata,test = "x2",undirected = F,alpha = alpha,strict = F,blacklist = bl,whitelist = wl)

# bnCS_IAMB_MI
bnCS_IAMB_MI<-iamb(traindata,test = "mi",undirected = F,alpha = alpha,strict = F,blacklist = bl,whitelist = wl)

#bnCS_HITON_PC_X2
bnCS_HITON_PC_X2<-si.hiton.pc(traindata,test = "x2",alpha = alpha,strict = F,blacklist = bl,whitelist = wl)

#bnCS_HITON_PC_MI
bnCS_HITON_PC_MI<-si.hiton.pc(traindata,test = "mi",alpha = alpha,strict = F,blacklist = bl,whitelist = wl)

#bnSB_HC_LL
bnSB_HC_LL<-hc(traindata,score="loglik",restart = restart,perturb = perturb,blacklist = bl,whitelist = wl)

#bnSB_HC_BIC
bnSB_HC_BIC<-hc(traindata,score="bic",restart = restart,perturb = perturb,blacklist = bl,whitelist = wl)

#bnSB_HC_AIC
bnSB_HC_AIC<-hc(traindata,score="aic",restart = restart,perturb = perturb,blacklist = bl,whitelist = wl)

#bnSB_HC_BDE
bnSB_HC_BDE<-hc(traindata,score="bde",restart = restart,perturb = perturb,blacklist = bl,whitelist = wl)

#bnSB_TABU_LL
bnSB_TABU_LL<-tabu(traindata,score="loglik",blacklist = bl,whitelist = wl)

#bnSB_TABU_BIC
bnSB_TABU_BIC<-tabu(traindata,score="bic",blacklist = bl,whitelist = wl)

#bnSB_TABU_AIC
bnSB_TABU_AIC<-tabu(traindata,score="aic",blacklist = bl,whitelist = wl)

#bnSB_TABU_BDE
bnSB_TABU_BDE<-tabu(traindata,score="bde",blacklist = bl,whitelist = wl)

bnH_MMHC_X2<-mmhc(traindata,test = "x2",alpha = alpha,strict = F,blacklist = bl,whitelist = wl,restart = restart,perturb = perturb)

bnH_MMHC_MI<-mmhc(traindata,test = "mi",alpha = alpha,strict = F,blacklist = bl,whitelist = wl,restart = restart,perturb = perturb)

#set modelNames
modelNames<-names(which(sapply(ls(),function(x) length(grep("bn",x)) > 0)))

#set models
models<-lapply(modelNames,get)
names(models)<-modelNames

#convert to dir
dirmodels<-lapply(models,function(x) {
	ifelse(!directed(x),net<-try(cextend(x,strict = F),silent=F),net<-x)
	;return(net)
	})
#rule out undirected models since there is no consistency in cextending them
dirmodels<-dirmodels[sapply(dirmodels,directed)]

#cvpreds: k-fold error prediction
cvpredsMean<-sapply(dirmodels,function(x) mean(sapply(nodes(x),function(tn) attr(bn.cv(preparedData,bn=x,loss="pred",loss.args=list(target=tn),fit="bayes",fit.args=list(iss=Bayesiss), method = "hold-out",m=round(datatestprop*nrow(preparedData)),k=kgroups),"mean"))))
cvpredsMean<-cvpredsMean[order(cvpredsMean)]
cvpredsMean<-data.frame(alg=names(cvpredsMean),predErr=as.numeric(cvpredsMean))

#fit models
fitmodels<-lapply(dirmodels,function(x) bn.fit(x,data = traindata[,which(names(traindata) %in% nodes(x))],method="bayes",iss=Bayesiss))

#nodes and arcs
nodesarcs<-t(sapply(fitmodels,function(x) c(length(nodes(x)),nrow(arcs(x)))))
nodesarcs<-data.frame(alg=rownames(nodesarcs),nodes=nodesarcs[,1],arcs=nodesarcs[,2])
#sort by arcs
nodesarcs<-nodesarcs[order(nodesarcs$arcs,decreasing = T),]
rownames(nodesarcs)<-NULL

## merge errPred
errscores <- merge(x = nodesarcs, y = cvpredsMean, all = FALSE, by = c("alg"))

#kld
KLDunsup<-mclapply(fitmodels,function(x) kld(x,testdata,smoothEmp),mc.cores=acores)
KLDunsup<-do.call(rbind,KLDunsup)
KLDunsup<-data.frame(alg=rownames(KLDunsup),kld=as.numeric(KLDunsup))
KLDunsup<-KLDunsup[order(KLDunsup$kld),]
rownames(KLDunsup)<-NULL

## merge KLD
kldscores <- merge(x = errscores, y = KLDunsup, all = FALSE, by = c("alg"))

#scores
sumLL<-sapply(dirmodels,function(x) bnlearn::score(x,testdata,type="loglik"))
#bic
bic<-sapply(dirmodels,function(x) bnlearn::score(x,testdata,type="bic"))
#aic
aic<-sapply(dirmodels,function(x) bnlearn::score(x,testdata,type="aic"))
#bde
bde<-sapply(dirmodels,function(x) bnlearn::score(x,testdata,type="bde"))
modelscores<-data.frame(alg=names(dirmodels),sumLL=sumLL,bic=bic,aic=aic,bde=bde)
modelscores <- merge(x = kldscores, y = modelscores, all = FALSE)

#rank
#first set precission in kld and predErr to 2 decimals
#since max precission we have in data is 1883,77
modelscores$predErr<-round(modelscores$predErr,2)
modelscores$kld<-round(modelscores$kld,2)
#do not rank kld
rankedscores<-cbind(modelscores[,c(1:5)],apply(modelscores[,-c(1:5)],2,function(v) sapply(v,function(x) which(x==sort(unique(v),decreasing = T)))))
#add error rankings
errorranks<-sapply(rankedscores$predErr,function(x) which(x==sort(unique(rankedscores$predErr))))
kldrank<-sapply(rankedscores$kld,function(x) which(x==sort(unique(rankedscores$kld))))
rankedscores<-cbind(rankedscores[,1:5],errorranks=errorranks,kldrank=kldrank,rankedscores[,-c(1:5)])
#totalranks is sum of errorranks+kldranks - min(errorranks+kldranks) + 1 
totalranks<-data.frame(rankedscores,totalrank=rowSums(with(rankedscores,rankedscores[,rankers]))-min(rowSums(with(rankedscores,rankedscores[,rankers])))+1)
totalranks<-totalranks[order(totalranks$totalrank),]
rownames(totalranks)<-NULL

#drop node column
totalranks<-totalranks[,-2]
totalranks<-totalranks[,c(1,2,5,6,7,8:11)]
#put BDE first
totalranks<-totalranks[c(2,1,3:nrow(totalranks)),]

#best representants
bestrepNames<-as.character(sapply(c("bnSB","bnCS","bnH"),function(v) as.character(totalranks$alg[which(sapply(totalranks$alg,function(x) length(grep(v,x))==1))[1]])))
#grab the real models
bestrep<-models[bestrepNames]
